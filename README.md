# Assignment (.Net Position)

### Cryptocurrency Converter

 * CryptocurrencyConverter-BE (.NET5)
 * CryptocurrencyConverter-FE (ReactJS version 16.14.0)

## Installation

### 1. CryptocurrencyConverter-BE

* Clone project from git.
```bash
 git clone https://ekyria@bitbucket.org/ekyria/cryptocurrencyconverter-be.git
```
* Run docker-compose.yml
```bash
docker-compose up --build
```
Please check if it works on your machine: [http://localhost:5000/swagger/index.html](http://localhost:5000/swagger/index.html).

There are exposed some needed APIs for the application.

### 2. CryptocurrencyConverter-FE

* Clone project from git.
```bash
 git clone https://ekyria@bitbucket.org/ekyria/cryptocurrencyconverter-fe.git
```
* Run docker-compose.yml
```bash
docker-compose up --build
```
Please check if it works on your machine: [http://localhost:3000/main](http://localhost:3000/main).

*note: please make sure that ports are available (5000, 3000, 3306).* 

#### Information

* CryptocurrencyConverter-BE creates a database called **cryptoConverter** that contains information about the price sources and its available currency pairs.