import React, { Component } from 'react';
import SideBar from '../../components/Navigation/Toolbar/SideBar';
import  {Navbar, Container, Row} from 'react-bootstrap';

class Layout extends Component {

  state = {
    isOpen : true,
    subMenu: [
        [
          {
            title: "Main",
            target: "main",
            containsNavBar: false
          }
        ]
      ]
    };

  toggle = () => 
  {
      this.setState(prevState => {
          return {isOpen: !prevState.isOpen};
      });
  }

  render() {

      let appBar;

      appBar = <SideBar
        isOpen = {this.state.isOpen} 
        subMenu = {this.state.subMenu}/>

      return (
        <div>
          <Navbar bg="dark" variant="dark" expand={false}>
            <Container fluid>
                <Navbar.Toggle onClick={this.toggle}/>
                <Navbar.Brand href="/main">
                    Cryptocurrency Converter
                </Navbar.Brand>
            </Container>
          </Navbar>
        
          <div className="App wrapper">
            {appBar}
            <Container className="p-2">
              <Row>
                {this.props.children}
              </Row>
            </Container>
          </div>
      </div>
      );
  }
}

export default Layout;