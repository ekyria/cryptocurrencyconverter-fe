import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { Container, Row, Col, Form, Button, FormGroup, FormControl } from 'react-bootstrap';

import Datatable from '../../components/Datatable/Datatable';
import * as actions from '../../store/actions/index';

class CryptocurrencyConverter extends Component {
    
    fetchData (event)  {
        event.preventDefault();        
        this.props.onLoadCryptoTickerData(event.target.priceSourceId.value, event.target.currencyPair.value);
    }

    componentDidMount() {
        this.props.onLoadPriceSource();
    }
    
    render() {
        
        let priceSourceOptions = <option value= {0} key={0}>Not available sources</option>;
        let currencyPairOptions =  <option value= {0} key={0}>Not available currency pairs</option>;
        let datatable;

        if (this.props.priceSources.length > 0) {
            
            priceSourceOptions = this.props.priceSources.map (item => (<option value={item.priceSourceId} key={item.priceSourceId}>{item.priceSourceName}</option>));
            currencyPairOptions= this.props.availableCurrencyPairs.map (item => (<option value={item.parameterRequest} key={item.parameterRequest}>{item.currencyPair}</option>));
        }

        if (this.props.data.length > 0) {

            datatable = <Datatable data={this.props.data}/>            
        }

        return (
            <Container fluid="md">
                <Row>
                    <Form onSubmit={(e) => this.fetchData(e)}>
                        <Row className="mb-3">
                            <Col md={3}>
                                <FormGroup as={Col} role="form">
                                    <FormControl as={Col} name="priceSourceId" className="form-control" as="select" onChange = { e => {

                                                                            console.log("e.target.value", e.target.value);
                                                                            this.props.onChangeSelectedPriceSource(e.target.value);
                                        
                                        }}> {priceSourceOptions} </FormControl>

                                </FormGroup>
                            </Col>
                            <Col md={3}>
                                <FormGroup as={Col} role="form">
                                    <FormControl as={Col} name="currencyPair" className="form-control" as="select" onChange={e => {

                                                                        console.log("e.target.value", e.target.value);
                                        
                                        }}> {currencyPairOptions} </FormControl>
                                </FormGroup>
                            </Col>
                            <Col md={1}>
                                <Button type="submit">Fetch</Button>
                            </Col>
                        </Row>
                    </Form>
                </Row>
                <Row>
                    <Col>
                        {datatable}
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.cryptoTicker.cryptoData,
        priceSources: state.priceSource.priceSources,
        selectedPriceSource: state.priceSource.selectedPriceSource,
        availableCurrencyPairs:  state.priceSource.availableCurrencyPairsPerSelectedPriceSource
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLoadCryptoTickerData: (priceSourceId, currencyPairParameter) => dispatch (actions.loadCryptoTickerData(priceSourceId, currencyPairParameter)),
        onLoadPriceSource: () => dispatch (actions.loadPriceSources()),
        onChangeSelectedPriceSource: (priceSourceId) => dispatch (actions.changeSelectedPriceSource(priceSourceId))
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(CryptocurrencyConverter);