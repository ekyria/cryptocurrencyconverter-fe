import axios from 'axios';
import * as actionTypes from './actionTypes';

export const updatePriceSources = (data) => {
    return {
        type : actionTypes.GET_PRICE_SOURCES,
        data : data
    };
};

export const changeSelectedPriceSource = (priceSourceId) => {
    console.log(priceSourceId);
    return {
        type : actionTypes.CHANGE_SELECTED_PRICE_SOURCE,
        priceSourceId: priceSourceId
    };
};

export const loadPriceSources = () => {
    return dispatch => {

        let url ='http://localhost:5000/api/v1/priceSource';

        axios.get(url, {
            "headers": {
                'Content-type': 'application/json'
        }
        })
        .then(response =>{

            console.log(response.data.items);
            dispatch(updatePriceSources(response.data));

        })
        .catch(error => {

            console.log(error.response);

        });
    }
}