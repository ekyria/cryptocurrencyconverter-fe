import axios from 'axios';
import * as actionTypes from './actionTypes';

export const updateCryptoData = (data) => {
    return {
        type : actionTypes.GET_CRYPTO_DATA,
        data : data
    };
};

export const loadCryptoTickerData = (priceSourceId, currencyPairParameter) => {
    return dispatch => {
        let url ='http://localhost:5000/api/v1/cryptoToFiat?priceSourceId=' + priceSourceId + '&currencyPairSymbol=' + currencyPairParameter;

        axios.get(url)
        .then(response =>{
           
            console.log(response.data);
            dispatch(updateCryptoData(response.data));
        })
        .catch(error => {
            console.log(error.response);
        });
    }
}
