import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    priceSources: [],
    selectedPriceSource: { priceSourceId: 0 },
    availableCurrencyPairsPerSelectedPriceSource: []
};

const getPriceSources = (state, action) => {
    return updateObject(state, { priceSources: action.data.items, selectedPriceSource: action.data.items[0], availableCurrencyPairsPerSelectedPriceSource: action.data.items[0].availableCurrencyPair});
}

const chageSelectedPriceSource = (state, action) => {

    const priceSourceId = parseInt(action.priceSourceId);
    const priceSource = state.priceSources.filter(obj => obj.priceSourceId === priceSourceId)[0];

    return updateObject(state, { availableCurrencyPairsPerSelectedPriceSource: priceSource.availableCurrencyPair } );
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_PRICE_SOURCES: return getPriceSources(state, action);
        case actionTypes.CHANGE_SELECTED_PRICE_SOURCE: return chageSelectedPriceSource(state, action);
        default:
            return state;
    }
}

export default reducer;