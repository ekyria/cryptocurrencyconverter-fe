import * as actionTypes from '../actions/actionTypes';

const initialState = {
    cryptoData:[]
};

const getCryptoData = (state, action) => {
    
    let data = {
        "id": state.cryptoData.length + 1,
        "price": action.data.items[0].price,
        "time": action.data.items[0].dateTime,
        "priceSourceName": action.data.items[0].priceSourceName,
        "currencyPair": action.data.items[0].currencyPair
    }
    
    return {...state, cryptoData: [...state.cryptoData, data] } 
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_CRYPTO_DATA: return getCryptoData(state, action);
        default:
            return state;
    }
}

export default reducer;