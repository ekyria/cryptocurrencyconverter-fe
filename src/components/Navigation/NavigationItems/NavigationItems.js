import React from 'react';

import NavigationItem from '../NavigationItem/NavigationItem';
import './NavigationItems.css';
import { NavItem } from 'reactstrap';

const NavigationItems = ( props ) => (
    <div>
        <NavItem key={props.id}>
            <NavigationItem link={props.link} title={props.title} />
        </NavItem>
        <Nav className="list-unstyled pb-3">
            <Nav.Link href="/">Main</Nav.Link>
        </Nav>
    </div>
);

export default NavigationItems;