import React from 'react';
import {Nav} from 'react-bootstrap';

const NavigationItem = props => (
    <Nav.Link href={props.link}>{props.title}</Nav.Link>
);

export default NavigationItem;