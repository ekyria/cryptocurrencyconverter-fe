import React from 'react';
import './Sidebar.css';
import classNames from 'classnames';
import SubMenu from '../../Navigation/Toolbar/SubMenu';


const SideBar = props => (
    <div className={classNames('sidebar', {'is-open': props.isOpen})}>
        <div className="sidebar-header">
            <h3>Menu</h3>
        </div>
        <div className="side-menu">
            <SubMenu title = "Menu" items = {props.subMenu[0]} />
        </div>
    </div>
    );

export default SideBar;