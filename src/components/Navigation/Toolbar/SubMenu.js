import React from 'react';
import NavigationItem from '../NavigationItem/NavigationItem';

const SubMenu = props => {

  const { items } = props;
  let newItem = [];

  items.forEach(element => {    
    if (!element.containsNavBar) {
      newItem.push({
        title: element.title,
        target: element.target,
        containsNavBar: false
      });
    }
  });   

  let navItem = newItem.map((item) => (
              <NavigationItem key = {item.target} link = {item.target} title = {item.title}/>              
              ));


  return (
    <div>
      {navItem}
    </div>
  );
}

export default SubMenu;