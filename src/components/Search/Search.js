
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min';


const MySearch = (props) => {

    const { SearchBar, ClearSearchButton } = Search;

    let input;
    const handleClick = () => {
      props.onSearch(input.value);
    };

    return (
      <div>
        <input
          className="form-control"
          style={ { backgroundColor: 'pink' } }
          ref={ n => input = n }
          type="text"
        />
        <button className="btn btn-warning" onClick={ handleClick }>Click to Search!!</button>
      </div>
    );
  };

  export default MySearch;