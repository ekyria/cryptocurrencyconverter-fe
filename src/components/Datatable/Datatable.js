import React from 'react';

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min';
import MySearch from '../Search/Search';

const Datatable = props => {
    
  const columns = [
    { dataField: 'id', text: 'Id', sort: true },
    { dataField: 'price', text: 'Price', sort: true },
    { dataField: 'time', text: 'Time', sort: true },
    { dataField: 'priceSourceName', text: 'Price Source', sort: true },
    { dataField: 'currencyPair', text: 'Currency Pair', sort: true }
  ];

  const defaultSorted = [{
  dataField: 'id',
  order: 'desc'
  }];


  const pagination = paginationFactory({
    page: 1,
    sizePerPage: 10,
    lastPageText: '>>',
    firstPageText: '<<',
    nextPageText: '>',
    prePageText: '<',
    showTotal: true,
    alwaysShowAllBtns: false,
    onPageChange: function (page, sizePerPage) {
      console.log('page', page);
      console.log('sizePerPage', sizePerPage);
    },
    onSizePerPageChange: function (page, sizePerPage) {
      console.log('page', page);
      console.log('sizePerPage', sizePerPage);
    }
  });

    const { SearchBar, ClearSearchButton } = Search;

    return (
        <ToolkitProvider
        bootstrap4
        keyField="id"
        data={ props.data }
        columns={ columns }
        search
      >
        {
          props => (
            <div>
              <SearchBar {...props.searchProps} srText = ""/>
              <ClearSearchButton {...props.searchProps} />
              <hr />
              <BootstrapTable defaultSorted={defaultSorted} pagination = {pagination} { ...props.baseProps } />
              <br />
            </div>
          )
        }
      </ToolkitProvider>
    );
};

export default Datatable