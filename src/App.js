import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Layout from './containers/Layout/Layout';
import CryptocurrencyConverter from './containers/CryptocurrencyConverter/CryptocurrencyConverter';

function App() {

  let routes = (
    <BrowserRouter>
      <Switch>
        <Route path="/main" exact component={CryptocurrencyConverter}/>
      </Switch>
    </BrowserRouter>
  )

  return (
      <Layout>
        {routes}
      </Layout>
  );
}

export default App;
